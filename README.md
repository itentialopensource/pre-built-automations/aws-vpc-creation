## _Deprecation Notice_

This Pre-Built Automation has been deprecated as of 12-13-2023 and will be end of life on 12-13-2024. The capabilities of this Pre-Built Automation have been replaced by the [Itential AWS - EC2 - REST Project](https://gitlab.com/itentialopensource/pre-built-automations/aws-ec2-rest).

# AWS VPC Creation

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

- [AWS VPC Creation](#aws-vpc-creation)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Installation Prerequisites](#installation-prerequisites)
  - [Features](#features)
  - [Future Enhancements](#future-enhancements)
  - [How to Install](#how-to-install)
  - [How to Run](#how-to-run)
    - [Subnets and Tags](#subnets-and-tags)
      - [Subnets](#subnets)
      - [Tags](#tags)
  - [Components](#components)
    - [Create Subnets](#create-subnets)
      - [Error Handling](#error-handling)
    - [Create Public Route Table](#create-public-route-table)
    - [Create Internet Gateway](#create-internet-gateway)
    - [Create Tags](#create-tags)
  - [Metrics](#metrics)
  - [Additional Information](#additional-information)

## Overview

The AWS VPC Creation pre-built enables users of the Itential Automation Platform to leverage the power of Amazon Web Services to create a virtual private cloud. The pre-built allows users to select the desired size of the VPC by specifying the CIDR block size, as well as specify a list of public and private subnets to create within the VPC.

<!-- Write a few sentences about the pre-built and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->
<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 2 min.

## Installation Prerequisites

Users must satisfy the following prerequisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2021.2`
* [EC2 Adapter](https://gitlab.com/itentialopensource/adapters/cloud/adapter-aws_ec2) instance for the region within which to create the VPC.
  * `^0.6.0`

## Features

The main benefits and features of the pre-built are outlined below.

<!-- Unordered list highlighting the most exciting features of the pre-built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->
* Allows creation of a single custom sized VPC, with an allowable CIDR block size of 16 / 24 / 28. This allows for a total address space size of 65536, 256 and 16 IP addresses, respectively.
* Enables creation of multiple subnets within the VPC. For each subnet the user may select a CIDR block size, availability zone, and designate the subnet as public or private. Public subnets will be connected to an internet gateway.
* Provides error handling for the failed creation of subnets. This gives the user an opportunity to adjust the CIDR block size and availability zone and then retry.
* Provides support for tagging all created resources with custom tags.
* Shows the pre/post diffs of information gathered from all VPCs in a selected region.
* Leverages AWS EC2 API, without any external dependencies like Ansible or Terraform.

## Future Enhancements

<!-- OPTIONAL - Mention if the pre-built will be enhanced with additional features on the road map -->
<!-- Ex.: This pre-built would support Cisco XR and F5 devices -->
In the future, this pre-built may be expanded to allow the creation of multiple VPCs within multiple regions.

## How to Install

To install the pre-built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section. 
* The pre-built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-built and click the install button.


## How to Run

Use the following to run the pre-built:

<!-- Explain the main entrypoint(s) for this pre-built: Automation Catalog item, Workflow, Postman, etc. -->
* This pre-built may be run from Operations Manager by clicking `Run` for the AWS VPC Creation item. The user will be presented with a form to select options to customize the VPC that will be created.
* Select the EC2 Adapter instance to use with the automation. The adapter instance that is selected will determine the AWS region within which the VPC will be created.
* Specify the first IPv4 address for the range of addresses to associate with the VPC.
* Select the CIRD block size, which may be one of three values:

    * `16`: 65,536 total addresses
    * `24`: 256 total addresses
    * `28`: 16 total addresses

* The number of available addresses may be smaller than listed above, as AWS reserves some addresses for internal use.


### Subnets and Tags

#### Subnets

To create a subnet, click the `+` button (bottom right-hand corner) of the Subnets table.

For each subnet, specify the CIDR block size. The CIDR block size may be one of three values:

* `16`: 65,536 total addresses
* `24`: 256 total addresses
* `28`: 16 total addresses

Of note, the total number of addresses allocated to the VPC cannot be exceeded. For example, if the VPC has a CIDR block size of 16, then only a single subnet with a CIDR block size of 16 may be created. The created subnets will be allocated and arranged so that the address spaces do not overlap.

Select whether the subnet is to be public or private. Public subnets will be associated with a route table attached to an internet gateway, while private subnets will be associated with the default route table for the VPC.

Optionally, an availability zone may be provided for each subnet. Check the availability zone offerings for the region in which the VPC is created.

#### Tags

To add a tag, click the `+` button (bottom right-hand corner) of the Tags table.

For each tag, specify a key-value pair. Tags will be added to all created resources, which includes:

* VPC
* Subnets
* Route tables
* Internet Gateway

## Components

The workflow for creating a VPC is broken up into child workflows for each stage of the creation process after the VPC itself has been created.

### Create Subnets

This workflow will create the specified public and private subnets. The address spaces for all subnets will be allocated so as not to overlap based upon the CIDR block size.

#### Error Handling

If a subnet fails to be created, the user will be presented with a form pre-populated with the CIDR block size and availability zone originally specified. The user may choose to either change the CIDR block size and / or availability zone to retry, or simply ignore the error to continue with the automation.


### Create Public Route Table

This workflow will create a new route table which will be associated with all created public subnets.

### Create Internet Gateway

This workflow will attach an internet gateway to the previously created route table. This will allow all designated public subnets to be reachable outside of the VPC over the internet.

### Create Tags

This workflow will tag _all_ resources created throughout the automation with the provided tags.

## Metrics

When the automation has finished running, the user will be provided with a diff that displays information about all VPCs in the region.


## Additional Information
Please use your Itential Customer Success account if you need support when using this pre-built. 
