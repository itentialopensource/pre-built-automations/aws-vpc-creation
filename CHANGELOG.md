
## 0.2.9 [03-18-2024]

* Update metadata.json

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!31

---

## 0.2.8 [02-05-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!27

---

## 0.2.7 [01-03-2024]

* Remove img tag from markdown file

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!23

---

## 0.2.6 [12-13-2023]

* Adds deprecation notice

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!21

---

## 0.2.5 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!20

---

## 0.2.4 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!19

---

## 0.2.3 [01-18-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!17

---

## 0.2.2 [07-09-2021]

* Update README.md, package.json files

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!16

---

## 0.2.1 [02-23-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!15

---

## 0.2.0 [06-18-2020]

* [minor/LB-404] Switch absolute path to relative

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!11

---

## 0.1.0 [06-17-2020]

* Update manifest to reflect gitlab name

See merge request itentialopensource/pre-built-automations/AWS-VPC-Creation!10

---

## 0.0.10 [04-30-2020]

* Updated Read.me file

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!9

---

## 0.0.9 [04-17-2020]

* Update package.json to contain "author"

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!8

---

## 0.0.8 [04-17-2020]

* Update package.json to contain "author"

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!8

---

## 0.0.7 [04-17-2020]

* Update package.json to contain "author"

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!8

---

## 0.0.6 [04-15-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!6

---

## 0.0.5 [04-09-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!4

---

## 0.0.4 [03-24-2020]

* Add readme and images

See merge request itentialopensource/pre-built-automations/staging/aws-vpc-creation!2

---

## 0.0.3 [03-24-2020]

* Add readme and images

See merge request itentialopensource/pre-built-automations/staging/aws-vpc-creation!2

---

## 0.0.2 [03-24-2020]

* Add readme and images

See merge request itentialopensource/pre-built-automations/staging/aws-vpc-creation!2

---\n\n\n\n\n\n
