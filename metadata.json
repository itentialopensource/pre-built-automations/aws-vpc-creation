{
  "name": "AWS VPC Creation",
  "webName": "AWS VPC Creation",
  "vendor": "Amazon Web Services",
  "product": "EC2",
  "osVersions": [],
  "apiVersions": [],
  "iapVersions": [
    "2021.1.x",
    "2021.2.x",
    "2022.1.x",
    "2023.1.x"
  ],
  "method": "REST",
  "type": "Workflow Project",
  "domains": [
    "Cloud"
  ],
  "tags": [
    "Cloud Networking",
    "Virtual Compute"
  ],
  "useCases": [
    "Add or remove Virtual Machine Images",
    "Instantiate, manage, remove Virtual Machine Instances",
    "Management of Cloud Network Assignments",
    "Deploy Virtual Services and Functions",
    "Add or remove Virtual Networks",
    "Provision, update, manage or remove Virtual Private Networks"
  ],
  "deprecated": {
    "isDeprecated": true,
    "deprecatedOn": "2023-12-13",
    "endOfLife": "2024-12-13",
    "replacedBy": {
      "name": "AWS - EC2 - REST",
      "webName": "AWS - Modular Workflows for EC2 Services",
      "isProductFeature": false,
      "overview": "This Pre-Built Automation bundle contains several example use cases that are applicable when Itential Automation Platform is integrated with the AWS EC2 REST API. Because every environment is different, these use cases are fully functioning examples that can be easily modified to operate in your specific environment. These workflows have been written with modularity in mind to make them easy to understand and simple to modify to suit your needs. Modular workflows in this project include:\n\n**Add Ingress Rule to Security Group - AWS** A modular workflow that automates adding an ingress rule to a security group.\n\n**Create and Attach Internet Gateway - AWS** A modular workflow that automates creating and attaching an internet gateway to a VPC in AWS.\n\n**Create EC2 Instance - AWS** A modular workflow that automates creating an EC2 instance in AWS.\n\n**Create Route - AWS** A modular workflow that automates creating a route within a VPC in AWS.\n\n**Create Security Group with Ingress Rules - AWS** A modular workflow that automates creating a security group with ingress rules in AWS.\n\n**Create VPC - AWS** A modular workflow that automates creating a VPC in AWS.\n\n**Create VPC Subnet - AWS** A modular workflow that automates creating a subnet for a VPC in AWS.\n\n**Provision VPC with Networking - AWS** A modular workflow that automates creating and configuring a VPC with networking in AWS.\n\n**Delete Security Groups by VPC - AWS** A modular workflow that automates deleting VPC Security groups in AWS.\n\n**Delete Subnets by VPC - AWS** A modular workflow that automates deleting subnets by VPC in AWS.\n\n**Destroy VPC and EC2 Instance - AWS** A modular workflow that automates destroying a VPC and EC2 Instance in AWS\n\n**Detach and Delete Internet Gateways by VPC - AWS** A modular workflow that automates detaching and deleting VPC internet gateways in AWS.",
      "repoLink": "https://gitlab.com/itentialopensource/pre-built-automations/aws-ec2-rest",
      "docLink": "",
      "webLink": ""
    }
  },
  "brokerSince": "",
  "documentation": {
    "storeLink": "",
    "npmLink": "",
    "repoLink": "https://gitlab.com/itentialopensource/pre-built-automations/aws-vpc-creation",
    "docLink": "",
    "demoLinks": [],
    "faqLink": "",
    "contributeLink": "",
    "issueLink": "",
    "webLink": "https://www.itential.com/automations/aws-vpc-creation/",
    "vendorLink": "https://aws.amazon.com/",
    "productLink": "https://aws.amazon.com/ec2/",
    "apiLinks": [
      "https://docs.aws.amazon.com/AWSEC2/latest/APIReference/Welcome.html"
    ]
  },
  "relatedItems": {
    "adapters": [
      {
        "name": "adapter-aws_ec2",
        "webName": "Amazon EC2",
        "overview": "AWS EC2",
        "isDependency": true,
        "repoLink": "https://gitlab.com/itentialopensource/adapters/cloud/adapter-aws_ec2",
        "docLink": "https://docs.itential.com/opensource/docs/amazon-aws-ec2",
        "webLink": "https://www.itential.com/adapters/amazon-ec2/",
        "configurationNotes": "In order to run the workflows in this Pre-Built Automation, the adapter property <code>xmlArrayKeys</code> must be added to the AWS EC2 adapter configuration with value as seen below:\n\n```json\n\"xmlArrayKeys\": [\n  \"item\"\n]\n```\n\nThis will ensure all response objects will set the data type of any instance of property <code>item</code> to an array, even if a single element is assigned to the property.",
        "versions": [
          "^0.7.2"
        ]
      }
    ],
    "integrations": [],
    "ecosystemApplications": [],
    "workflowProjects": [
      {
        "name": "AWS - EC2 - REST",
        "webName": "AWS - Modular Workflows for EC2 Services",
        "isProductFeature": false,
        "overview": "This Pre-Built Automation bundle contains several example use cases that are applicable when Itential Automation Platform is integrated with the AWS EC2 REST API. Because every environment is different, these use cases are fully functioning examples that can be easily modified to operate in your specific environment. These workflows have been written with modularity in mind to make them easy to understand and simple to modify to suit your needs. Modular workflows in this project include:\n\n**Add Ingress Rule to Security Group - AWS** A modular workflow that automates adding an ingress rule to a security group.\n\n**Create and Attach Internet Gateway - AWS** A modular workflow that automates creating and attaching an internet gateway to a VPC in AWS.\n\n**Create EC2 Instance - AWS** A modular workflow that automates creating an EC2 instance in AWS.\n\n**Create Route - AWS** A modular workflow that automates creating a route within a VPC in AWS.\n\n**Create Security Group with Ingress Rules - AWS** A modular workflow that automates creating a security group with ingress rules in AWS.\n\n**Create VPC - AWS** A modular workflow that automates creating a VPC in AWS.\n\n**Create VPC Subnet - AWS** A modular workflow that automates creating a subnet for a VPC in AWS.\n\n**Provision VPC with Networking - AWS** A modular workflow that automates creating and configuring a VPC with networking in AWS.\n\n**Delete Security Groups by VPC - AWS** A modular workflow that automates deleting VPC Security groups in AWS.\n\n**Delete Subnets by VPC - AWS** A modular workflow that automates deleting subnets by VPC in AWS.\n\n**Destroy VPC and EC2 Instance - AWS** A modular workflow that automates destroying a VPC and EC2 Instance in AWS\n\n**Detach and Delete Internet Gateways by VPC - AWS** A modular workflow that automates detaching and deleting VPC internet gateways in AWS.",
        "repoLink": "https://gitlab.com/itentialopensource/pre-built-automations/aws-ec2-rest",
        "versions": [
          "^1.0.0"
        ],
        "docLink": "",
        "webLink": ""
      }
    ],
    "transformationProjects": [],
    "exampleProjects": []
  }
}